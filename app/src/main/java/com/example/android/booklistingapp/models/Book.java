package com.example.android.booklistingapp.models;

import android.graphics.Bitmap;

/**
 * An instance of this class stores information about a Google Book.
 */

public class Book {

    private String title;
    private String[] authors;
    private String description;
    private Bitmap image;
    private String httpLink;

    public Book(String title, String[] authors, String description,
                Bitmap image, String httpLink) {
        this.title = title;
        this.authors = authors;
        this.description = description;
        this.image = image;
        this.httpLink = httpLink;
    }

    public String getTitle() {
        return title;
    }

    public String[] getAuthors() {
        return authors;
    }

    public String getDescription() {
        return description;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getHttpLink() {
        return httpLink;
    }
}