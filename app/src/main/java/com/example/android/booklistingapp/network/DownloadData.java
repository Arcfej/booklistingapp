package com.example.android.booklistingapp.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.example.android.booklistingapp.models.Book;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * This class contains all the methods which download data from the internet.
 */

public class DownloadData {

    private static final String LOG_TAG = DownloadData.class.getSimpleName();

    //region Google Books API URIs, URLs and parameters
    private static final String BASE_API_URI_STRING = "https://www.googleapis.com/books/v1/volumes";
    private static final String API_KEY_PARAMETER = "key";
    private static final String API_KEY = "AIzaSyD_7EOUNT22Px560oKhHtm6pLWUImzbn-8";
    private static final String QUERY_PARAMETER = "q";
    private static final String PARTIAL_RESPONSE_PARAMETER = "fields";
    private static final String PARTIAL_RESPONSE_FIELDS = "items/volumeInfo(title,authors,description,imageLinks/thumbnail,infoLink)";
    //endregion

    /**
     * Build a URL with the given query, make a HttpUrlConnection to the Google Books API servers
     * and return the results in a List of Book objects.
     *
     * @param query is the String the user want to search for
     * @return an empty list if the query is "" or null, and a list of Book object if there is a
     * result from the server
     */
    public static List<Book> search(String query) {
        if (TextUtils.isEmpty(query)) {
            return new ArrayList<>();
        }
        List<Book> list = new ArrayList<>();
        try {
            URL searchUrl = buildSearchUrl(query);
            String jsonResult = downloadData(searchUrl);
            list = fetchBookData(jsonResult);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Cannot create URL");
        } catch (IOException e) {
            Log.e(LOG_TAG, "Cannot connect to the server.");
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Failed to parse JSONObject of -Array");
        }
        return list;
    }

    /**
     * Build a URL with the given String as query,
     * for searching books on the Google Books API servers.
     *
     * @param query to search upon
     * @return the built URL
     * @throws MalformedURLException if the URL cannot be built.
     */
    private static URL buildSearchUrl(String query) throws MalformedURLException {
        Uri builtUri = Uri.parse(BASE_API_URI_STRING)
                .buildUpon()
                .appendQueryParameter(API_KEY_PARAMETER, API_KEY)
                .appendQueryParameter(QUERY_PARAMETER, query)
                .appendQueryParameter(PARTIAL_RESPONSE_PARAMETER, PARTIAL_RESPONSE_FIELDS)
                .build();
        return new URL(builtUri.toString());
    }

    /**
     * Download the data from the given URL.
     *
     * @param searchUrl to download from
     * @return the downloaded data in String format
     * @throws IOException if it cannot connect to the server.
     */
    private static String downloadData(URL searchUrl) throws IOException {
        if (TextUtils.isEmpty(searchUrl.toString())) {
            return "";
        }
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        Scanner scanner = null;
        String result = "";
        try {
            connection = (HttpURLConnection) searchUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(20000);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                inputStream = connection.getInputStream();
                scanner = new Scanner(inputStream).useDelimiter("\\A");
                result = scanner.next();
            } else {
                Log.e(LOG_TAG, "Error response code: " + responseCode);
                Log.e(LOG_TAG, "Error response message: " + connection.getResponseMessage());
            }
        } finally {
            // Release the resources
            if (scanner != null) {
                scanner.close();
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Failed to close stream");
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    /**
     * Create a list of Book object from a String data stream.
     * If the stream doesn't contain data for a book, set it to a default value,
     * like an empty String of null object.
     *
     * @param jsonResult is the data stream which contains the books data.
     * @return The list of Books
     * @throws JSONException if it's fails to parse a JSONObject or JSONArray.
     */
    private static List<Book> fetchBookData(String jsonResult) throws JSONException {
        if (TextUtils.isEmpty(jsonResult)) {
            return new ArrayList<>();
        }
        List<Book> list = new LinkedList<>();
        JSONObject data = new JSONObject(jsonResult);
        JSONArray items = data.getJSONArray("items");
        for (int i = 0; i < items.length(); i++) {
            JSONObject volume = items.getJSONObject(i);
            JSONObject volumeInfo = volume.getJSONObject("volumeInfo");
            String httpLink = volumeInfo.optString("infoLink");
            String title = volumeInfo.optString("title");
            String[] authors = null;
            if (volumeInfo.has("authors")) {
                JSONArray authorsJSON = volumeInfo.getJSONArray("authors");
                authors = new String[authorsJSON.length()];
                for (int j = 0; j < authorsJSON.length(); j++) {
                    authors[j] = authorsJSON.optString(j);
                }
            }
            String description = volumeInfo.optString("description");
            String image = null;
            if (volumeInfo.has("imageLinks")) {
                JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");
                image = imageLinks.optString("thumbnail");
            }
            Book book = new Book(title, authors, description,
                    downloadImage(buildImageUrl(image)), httpLink);
            Log.d(LOG_TAG, book.toString());
            list.add(book);
        }
        return list;
    }

    /**
     * Build an URL from the given String.
     *
     * @param urlString String format of the URL
     * @return the built URL
     */
    private static URL buildImageUrl(String urlString) {
        if (TextUtils.isEmpty(urlString)) {
            return null;
        }
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Wrong url for the image");
        }
        return url;
    }

    /**
     * Download an image from the given URL
     *
     * @param url of the image
     * @return the image in Bitmap format
     */
    private static Bitmap downloadImage(URL url) {
        if (url == null) {
            return null;
        }
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        Bitmap result = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(20000);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                inputStream = connection.getInputStream();
                result = BitmapFactory.decodeStream(inputStream);
            } else {
                String responseMessage = connection.getResponseMessage();
                Log.e(LOG_TAG, "Error response code: " + responseCode);
                Log.e(LOG_TAG, "Error response message: " + responseMessage);
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Cannot connect to the server");

        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Failed to close stream");
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    /**
     * Check if the system has internet connection or not.
     *
     * @param context of the application
     * @return true if it has internet connection.
     */
    public static boolean hasConnection(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}