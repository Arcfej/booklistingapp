package com.example.android.booklistingapp.activities;

import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.booklistingapp.R;
import com.example.android.booklistingapp.adapters.BookAdapter;
import com.example.android.booklistingapp.models.Book;
import com.example.android.booklistingapp.network.DownloadData;

import java.util.ArrayList;
import java.util.List;

/**
 * Display a list of search results about books and instructions to the user how to search.
 * Uses the Google Books API.
 */
public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Book>> {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int BOOK_LOADER_TASK_ID = 100;
    private static final String LIST_INSTANCE_STATE = "BOOK_LIST";
    private static final String SEARCH_QUERY_INSTANCE_STATE = "SEARCH_QUERY";
    private static final String LIST_POSITION_INSTANCE_STATE = "LIST_POSITION";

    private String searchQuery;

    private BookAdapter adapter;
    private ListView bookList;
    private TextView emptyListView;
    private ProgressBar loadingIndicator;
    private TextView makeSearchMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //region Find views
        bookList = findViewById(R.id.book_list);
        emptyListView = findViewById(R.id.empty_list_view);
        loadingIndicator = findViewById(R.id.loading_indicator);
        makeSearchMessage = findViewById(R.id.make_search_message);
        //endregion
        searchQuery = "";
        adapter = new BookAdapter(this, new ArrayList<Book>());
        bookList.setAdapter(adapter);
        bookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // Open the webpage of the book on Google Books
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = adapter.getItem(position).getHttpLink();
                if (!TextUtils.isEmpty(url)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });
        bookList.setEmptyView(emptyListView);
        // Restore the list and the scrolling position to it's previous state.
        if (savedInstanceState != null) {
            bookList.onRestoreInstanceState(savedInstanceState.getParcelable(LIST_INSTANCE_STATE));
            searchQuery = savedInstanceState.getString(SEARCH_QUERY_INSTANCE_STATE, "");
            bookList.setSelectionFromTop(savedInstanceState.getInt(LIST_POSITION_INSTANCE_STATE, 0), 0);
            handleIntent(getIntent());
        }
    }

    /**
     * This activity is the search activity. Every time the user enter a new search query, the
     * activity receives the intent here and pass it to the handleIntent() method.
     *
     * @param intent which contains the new search query.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        emptyListView.setText("");
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Shows the already downloaded list,
     * or starts the download in the background based on the search query,
     * or show a message if there isn't any internet connection.
     *
     * @param intent which contains the search query.
     */
    private void handleIntent(Intent intent) {
        makeSearchMessage.setVisibility(View.GONE);
        String newQuery = intent.getStringExtra(SearchManager.QUERY);
        if (newQuery == null) {
            newQuery = "";
        }
        if (newQuery.equals(searchQuery)) {
            getLoaderManager().initLoader(BOOK_LOADER_TASK_ID, null, this);
        } else {
            if (DownloadData.hasConnection(getApplicationContext())) {
                searchQuery = newQuery;
                getLoaderManager().restartLoader(BOOK_LOADER_TASK_ID, null, this);
            } else {
                Toast noInternetMessage =
                        Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_SHORT);
                noInternetMessage.setGravity(Gravity.CENTER, 0, 0);
                noInternetMessage.show();
            }
        }
    }

    /**
     * Create a new Loader to download the list of the searched books' data
     * and display the Loading process to the user.
     *
     * @param id   of the Loader (not used)
     * @param args (not used)
     * @return the new Loader.
     */
    @Override
    public BookLoader onCreateLoader(int id, Bundle args) {
        showLoading();
        return new BookLoader(this, searchQuery);
    }

    /**
     * Show the loading indicator and hide the list.
     */
    private void showLoading() {
        loadingIndicator.setVisibility(View.VISIBLE);
        bookList.setVisibility(View.INVISIBLE);
    }

    /**
     * When the data downloaded pass it to the ListView's adapter to display it
     * and show the list instead of the loading indicator.
     *
     * @param loader which downloaded the data
     * @param list   of the books
     */
    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> list) {
        showList();
        adapter.clear();
        adapter.addAll(list);
        emptyListView.setText(getString(R.string.no_result));
    }

    /**
     * Show the list of books and hide the loading indicator.
     */
    private void showList() {
        loadingIndicator.setVisibility(View.INVISIBLE);
        bookList.setVisibility(View.VISIBLE);
    }

    /**
     * Clear all the data when the
     *
     * @param loader is reset.
     */
    @Override
    public void onLoaderReset(Loader loader) {
        adapter.clear();
    }

    /**
     * Custom Loader to download info about books based on the query the user's typed in.
     */
    private static class BookLoader extends AsyncTaskLoader<List<Book>> {

        private String searchQuery;

        /**
         * Basic constructor.
         *
         * @param context     of the data
         * @param searchQuery the user has typed in.
         */
        BookLoader(Context context, String searchQuery) {
            super(context);
            this.searchQuery = searchQuery;
        }

        /**
         * Force downloading when Loader initialized.
         */
        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        /**
         * Start the downloading in a background thread.
         *
         * @return a list of Books
         */
        @Override
        public List<Book> loadInBackground() {
            return DownloadData.search(searchQuery);
        }

    }

    /**
     * Inflate the layout for the menu of this activity.
     *
     * @param menu of the activity
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Goes through the menu items and checks which one is selected.
     * In case:
     * - search_actionbar: the user initiated a search.
     *
     * @param item which has been selected.
     * @return true if one's selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.search_actionbar) {
            onSearchRequested();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Saves the state and position of the list, and the last searched query.
     *
     * @param outState of the activity
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(LIST_INSTANCE_STATE, bookList.onSaveInstanceState());
        outState.putString(SEARCH_QUERY_INSTANCE_STATE, searchQuery);
        outState.putInt(LIST_POSITION_INSTANCE_STATE, bookList.getFirstVisiblePosition());
        super.onSaveInstanceState(outState);
    }
}