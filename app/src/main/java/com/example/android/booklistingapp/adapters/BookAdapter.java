package com.example.android.booklistingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.booklistingapp.R;
import com.example.android.booklistingapp.models.Book;

import java.util.List;

/**
 * This adapter displays a list of books to the user.
 */

public class BookAdapter extends ArrayAdapter<Book> {

    /**
     * Required constructor. It's just call super.
     *
     * @param context of the list
     * @param list    to display
     */
    public BookAdapter(@NonNull Context context, List<Book> list) {
        super(context, 0, list);
    }

    /**
     * Get the view to display next in the list. If there isn't one, inflate it.
     * Update the view with data.
     *
     * @param position    of the next view
     * @param convertView is the next view. It's null if there isn't one.
     * @param parent      of the convertView
     * @return the updated or inflated view.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item, parent, false);
        }
        setViews(position, convertView);
        return convertView;
    }

    /**
     * Fill the list item with data:
     * - image: if there isn't a downloaded image show and empty grey rectangle.
     * - authors:     \
     * - title:        } if there isn't text to display, hide the view from displaying.
     * - description: /
     *
     * @param position    of the list item
     * @param convertView is the list item to update.
     */
    private void setViews(int position, View convertView) {
        ImageView image = convertView.findViewById(R.id.image);
        TextView authors = convertView.findViewById(R.id.authors);
        TextView title = convertView.findViewById(R.id.title);
        TextView description = convertView.findViewById(R.id.description);
        Book item = getItem(position);
        if (item != null) {
            if (item.getImage() != null) {
                image.setImageBitmap(item.getImage());
            } else {
                image.setImageResource(R.drawable.empty_book_cover);
            }
            if (item.getAuthors() != null) {
                authors.setText(fetchAuthors(item.getAuthors()));
            } else {
                authors.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(item.getTitle())) {
                title.setText(item.getTitle());
            } else {
                title.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(item.getDescription())) {
                description.setText(item.getDescription());
            } else {
                description.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Make from an array of authors a single line text.
     *
     * @param array of the authors
     * @return the authors in one String divided by commas, if there are more than one.
     */
    private String fetchAuthors(String[] array) {
        if (array == null) {
            return "";
        }
        StringBuilder authors = new StringBuilder(array[0]);
        if (array.length > 1) {
            for (int i = 1; i < array.length; i++) {
                authors.append(", ").append(array[i]);
            }
        }
        return authors.toString();
    }
}